var studentsAndPoints = [
		'Алексей Петров', 0,
		'Ирина Овчинникова', 60,
		'Глеб Стукалов', 30,
		'Антон Павлович', 30,
		'Виктория Заровская', 30,
		'Алексей Левенец', 70,
		'Тимур Вамуш', 30,
		'Евгений Прочан', 60,
		'Александр Малов', 0
	],
	studentMaxPoints,
	maxPoints;

var students = studentsAndPoints.filter(function (item, i) {
		return i % 2 == 0;
	}),
	points   = studentsAndPoints.filter(function (item, i) {
		return i % 2 == 1;
	});

console.log('Список студентов:');
points.forEach(function (point, i) {
	console.log('Студент %s набрал %d баллов', students[i], point);
	if (!maxPoints || point > maxPoints ) {
		studentMaxPoints = students[i];
		maxPoints        = point;
	}
});

console.log('\nСтудент набравший максимальный балл: %s (%d баллов)', studentMaxPoints, maxPoints);

points = points.map(function (point, i) {
	return (students[i] == 'Ирина Овчинникова' || students[i] == 'Александр Малов') ? point + 30 : point;
});

function getTop(num) {
	var temp = [],
		sortStudentsAndPoints = [];
	points.forEach(function (point, i) {
		temp[i] = [students[i], point];
	});
	temp.sort(function(a, b) {
		return b[1] - a[1];
	});
	temp = temp.slice(0, num);
	temp.forEach(function (arr, i) {
		sortStudentsAndPoints.push(arr[0], arr[1]);
	});
	return sortStudentsAndPoints;
}

function printTop(num) {
	topStudentsAndPoints = getTop(num);
	console.log('\nТоп ' + num + ':');
	for (i = 0, imax = topStudentsAndPoints.length / 2; i < imax; i++) {
		console.log('%s - %d баллов', topStudentsAndPoints[i * 2], topStudentsAndPoints[i * 2 + 1]);
	}
}

printTop(3);
printTop(5);